# Population Projection

A cohort-component model that estimates China 's population at a provincial level under different scenarios of total fertility rate (TFR).